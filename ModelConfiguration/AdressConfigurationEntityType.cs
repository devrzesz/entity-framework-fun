﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity.ModelConfiguration;

namespace EF6ForFun
{
    class AdressConfigurationEntityType : ComplexTypeConfiguration<Address>
    {
        public AdressConfigurationEntityType()
        {
            Property(p => p.PostalCode).HasColumnName("Kod pocztowy");
            Ignore(p => p.Street);
        }
    }
}
