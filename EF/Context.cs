﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF6ForFun
{
    public class Context : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Shipment> Shipments { get; set; }


        public Context()
            : base ("name=DefaultConnection")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // modelBuilder.Configurations.Add<Address>(new AdressConfigurationEntityType());
            modelBuilder.Entity<User>()
                .HasOptional(u => u.BillingAddress)
                .WithRequired()
                .WillCascadeOnDelete(true);
            modelBuilder.Entity<Shipment>()
                .HasRequired(u => u.DeliveryAddress)
                .WithOptional()
                .WillCascadeOnDelete(true);


            base.OnModelCreating(modelBuilder);
        }

    }
}
