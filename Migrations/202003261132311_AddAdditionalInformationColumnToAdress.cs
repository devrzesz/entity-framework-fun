﻿namespace EF6ForFun.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAdditionalInformationColumnToAdress : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "Address_AdditionalInformation", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Users", "Address_AdditionalInformation");
        }
    }
}
