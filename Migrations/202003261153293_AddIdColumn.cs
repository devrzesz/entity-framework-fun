﻿namespace EF6ForFun.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddIdColumn : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Users", "Address_Id", c => c.Int(nullable: false));
            AlterColumn("dbo.Users", "Address_City", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Users", "Address_City", c => c.String(nullable: false));
            DropColumn("dbo.Users", "Address_Id");
        }
    }
}
