﻿namespace EF6ForFun.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LittleChangesOnAdressType : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Users", name: "Address_PostalCode", newName: "Kod pocztowy");
            AlterColumn("dbo.Users", "Address_City", c => c.String(nullable: false));
            DropColumn("dbo.Users", "Address_Street");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Users", "Address_Street", c => c.String());
            AlterColumn("dbo.Users", "Address_City", c => c.String());
            RenameColumn(table: "dbo.Users", name: "Kod pocztowy", newName: "Address_PostalCode");
        }
    }
}
