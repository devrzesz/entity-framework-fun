﻿namespace EF6ForFun.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddShipmentTableAndAddressAsWell : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Addresses",
                c => new
                    {
                        AddressId = c.Int(nullable: false),
                        Street = c.String(),
                        City = c.String(),
                        PostalCode = c.String(),
                    })
                .PrimaryKey(t => t.AddressId)
                .ForeignKey("dbo.Users", t => t.AddressId)
                .Index(t => t.AddressId);
            
            CreateTable(
                "dbo.Shipments",
                c => new
                    {
                        ShipmentId = c.Int(nullable: false),
                        CreatedOn = c.DateTime(nullable: false),
                        State = c.String(),
                    })
                .PrimaryKey(t => t.ShipmentId)
                .ForeignKey("dbo.Addresses", t => t.ShipmentId)
                .Index(t => t.ShipmentId);
            
            DropColumn("dbo.Users", "Username");
            DropColumn("dbo.Users", "Address_Id");
            DropColumn("dbo.Users", "Address_City");
            DropColumn("dbo.Users", "Kod pocztowy");
            DropColumn("dbo.Users", "Address_AdditionalInformation");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Users", "Address_AdditionalInformation", c => c.String());
            AddColumn("dbo.Users", "Kod pocztowy", c => c.String());
            AddColumn("dbo.Users", "Address_City", c => c.String());
            AddColumn("dbo.Users", "Address_Id", c => c.Int(nullable: false));
            AddColumn("dbo.Users", "Username", c => c.String());
            DropForeignKey("dbo.Addresses", "AddressId", "dbo.Users");
            DropForeignKey("dbo.Shipments", "ShipmentId", "dbo.Addresses");
            DropIndex("dbo.Shipments", new[] { "ShipmentId" });
            DropIndex("dbo.Addresses", new[] { "AddressId" });
            DropTable("dbo.Shipments");
            DropTable("dbo.Addresses");
        }
    }
}
