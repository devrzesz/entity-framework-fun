﻿namespace EF6ForFun.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ApplyCasadeDelete : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Shipments", "ShipmentId", "dbo.Addresses");
            DropForeignKey("dbo.Addresses", "AddressId", "dbo.Users");
            AddForeignKey("dbo.Shipments", "ShipmentId", "dbo.Addresses", "AddressId", cascadeDelete: true);
            AddForeignKey("dbo.Addresses", "AddressId", "dbo.Users", "UserId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Addresses", "AddressId", "dbo.Users");
            DropForeignKey("dbo.Shipments", "ShipmentId", "dbo.Addresses");
            AddForeignKey("dbo.Addresses", "AddressId", "dbo.Users", "UserId");
            AddForeignKey("dbo.Shipments", "ShipmentId", "dbo.Addresses", "AddressId");
        }
    }
}
