﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF6ForFun
{
    public class Shipment
    {
        public int ShipmentId { get; set; }
        public DateTime CreatedOn { get; set; }
        public string State { get; set; }
        public virtual Address DeliveryAddress { get; set; }
    }
}
